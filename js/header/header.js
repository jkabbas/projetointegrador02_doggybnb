
//check-session.php

$.get("/controller/check-session.php", function(data){
    data = JSON.parse(data);
    let pathname = '';
    console.log(data);
    if(data.session) {
        pathname = window.location.pathname;
        let url = '/home.html';
        if(pathname == url) {
            //set header
            $.get("js/header/header-home.html", function(data){
                $("div#header").html(data);
            });
        } else if (pathname == '/add-friends.html'){
            $.get("js/header/header-add-friends.html", function(data){
                $("div#header").html(data);
            });
        }else {
            window.location = url;
        }
    } else {
        pathname = window.location.pathname;
        let url = '/index.html';
        if(pathname == '/index.html') {
            $.get("js/header/header-index.html", function(data){
                $("div#header").html(data);
            });
        } else if (pathname == '/usuario_cadastro.html' || pathname == '/usuario_cadastro_resposta.html') {
            $.get("js/header/header-usuario-cadastro.html", function(data){
                $("div#header").html(data);
            });
        } else {
            window.location = url;
        }
    }
});
