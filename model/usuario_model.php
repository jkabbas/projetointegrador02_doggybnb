<?php
    class Usuario
    {
        function Cadastro($username, $name, $passwordHash, $saltHash, $phone, $email, $address)
        {     
            $objDb = new db();
            $link = $objDb->mysqlConnect();
    
            $stmt = $link->prepare("INSERT INTO usuario(username, name, passwordhash, salthash) VALUES (?, ?, ?, ?);");
            $stmt->bind_param("ssss", $username, $name, $passwordHash, $saltHash);
            $runquery = $stmt->execute();
    
            if($runquery)
            {
                $id = $link->insert_id;
                $stmt2 = $link->prepare("INSERT INTO contato(telefone, email, endereco, userid) VALUES (?, ?, ?, ?);");
                $stmt2->bind_param("ssss", $phone, $email, $address, $id);
                $runquery2 = $stmt2->execute();
                if($runquery2) {
                    return "Sucesso ao cadastrar usuário!";
                }
                else {
                    return "Falha ao cadastrar usuário: ".mysqli_error($link);
                }
            }
            else {
                return "Falha ao cadastrar usuário: ".mysqli_error($link);
            }
        }
        
        function get_result($statement)
        {
            $result = array();
            $statement->store_result();
            for ($i = 0; $i < $statement->num_rows; $i++)
            {
                $metadata = $statement->result_metadata();
                $params = array();
                while ($field = $metadata->fetch_field())
                {
                    $params[] = &$result[$i][$field->name];
                }
                call_user_func_array(array($statement, 'bind_result'), $params);
                $statement->fetch();
            }
            return $result;
        }

        function Autenticacao($username, $password)
        {
            $objDb = new db();
            $link = $objDb->mysqlConnect();

            $stmt = $link->prepare("SELECT id, username, passwordhash, salthash FROM usuario WHERE username = ?;");
            $stmt->bind_param("s", $username);
            $runquery = $stmt->execute();


            //$result = $stmt->get_result(); // Não pode ser usado sem o mysqlnd NATIVE DRIVER
            $result = $this->get_result($stmt);

            if ($runquery) {
                if ($row = array_shift($result)) {
                    if ($row['passwordhash'] == md5($password . $row['salthash'])) {
                        session_start();
                        $_SESSION['userid'] = $row['id'];
                        header('Location: ../home.html');
                        exit();
                    } else {
                        return "Senha incorreta!";
                    }
                } else {
                    return "Usuario não encontrado" . mysqli_error($link);
                }
            }
        }
    }
?>